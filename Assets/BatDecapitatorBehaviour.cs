﻿using UnityEngine;
using System.Collections;

public class BatDecapitatorBehaviour : StateMachineBehaviour {

    public bool SwitchOnEnter = true;
    public bool SwitchOnExit = true;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if(SwitchOnEnter)
		    SetBats (animator, false, true);

	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if(SwitchOnExit)
		    SetBats (animator, true, false);
	}


	void SetBats(Animator animator, bool back, bool hand)
	{
		Transform parent = animator.transform.parent;

		if (parent != null) {
			PriestMovement movement = parent.GetComponent<PriestMovement> ();
			if (movement != null) {
				if (movement.BatBack != null)
					movement.BatBack.gameObject.SetActive (back);

				if (movement.batHand != null)
					movement.batHand.gameObject.SetActive (hand);
			}
		}
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
