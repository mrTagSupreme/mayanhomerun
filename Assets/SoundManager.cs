﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance;

	public AudioSource pressAnyKey;

	public AudioSource decapitate;

	public AudioSource headoff;

	public AudioSource homerun;

	public AudioSource[] demon;

	public AudioSource[] combos;

	public AudioSource[] hits;

	void OnEnable()
	{
		instance = this;
	}

	public void Play(AudioSource source)
	{
		if (source != null) {
			//Debug.Log ("Play: " + source.name);
			source.Play ();
		}
	}

	public void Play(AudioSource[] sources)
	{
		if (sources.Length > 0) {
			AudioSource source = sources [Random.Range (0, sources.Length - 1)];
			if (source != null) {
				//Debug.Log ("PlayRandom: " + source.name);
				source.Play ();
			}
		}
	}

	public void Play(AudioSource[] sources, float lerpPercentage)
	{
		if (sources.Length > 0) {

			lerpPercentage = lerpPercentage < 0f ? 0f : lerpPercentage > 1f ? 1f : lerpPercentage;

			AudioSource source = sources [(int)Mathf.Round((float)(sources.Length - 1) * lerpPercentage)];
			if (source != null) {
				//Debug.Log ("PlayRandom: " + source.name);
				source.Play ();
			}
		}
	}
}
