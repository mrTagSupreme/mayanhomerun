﻿using UnityEngine;
using System.Collections;

public class ComboParticle : MonoBehaviour {

	public Vector3 direction;

	public float lifetime = 3f;

	protected float _timer = 0f;

	void Start () 
	{
		_timer = lifetime;
	}

	void Update () {
		this.transform.position += MayaTime.deltaTime * direction;

		_timer -= MayaTime.deltaTime;

		if (_timer < 0f)
			Destroy (gameObject);
	}
}
