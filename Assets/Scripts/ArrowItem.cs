﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ArrowItem : MonoBehaviour {

	protected Text _text;

	void Start () 
	{
		_text = GetComponentInChildren<Text> ();
	}
	
	public void UpdateText(float distance)
	{
		if (_text != null)
			_text.text = distance.ToString ("F") + " m";
	}
}
