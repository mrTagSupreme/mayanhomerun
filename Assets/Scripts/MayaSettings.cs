﻿using UnityEngine;
using System.Collections;

public class MayaSettings : MonoBehaviour {

	public int maxEnemies = 10;

	public static MayaSettings instance;


	protected bool invalidatePaused = false;
	protected bool invalidatePausedValue = false;

	void OnEnable()
	{
		instance = this;
	}

	void Update() {
		if (invalidatePaused == true) {
			_paused = invalidatePausedValue;
			invalidatePaused = false;
		}
		
	}

	protected static bool _paused = false;

	protected static bool _tutorialPaused = false;


	public static bool paused
	{
		get { return _paused; }
		set { if (instance != null) {
				instance.invalidatePaused = true; 
				instance.invalidatePausedValue = value;
			}}
	}

	public static bool tutorialPaused {
		get { return _tutorialPaused; }
		set { _tutorialPaused = value; }
	}
		

	public static bool playerMovementEnabled
	{
		get { return paused == false && _tutorialPaused == false; }
	}

	public static bool enemyMovementEnabled
	{
		get { return paused == false && _tutorialPaused == false; }
	}
}
