﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour 
{
	protected string _waitForInput = null;

	protected GameObject _currentInstance;


	void Start () {
	
	}

	void Update () 
	{
		if (_waitForInput != null) {
			if (Input.GetButtonDown (_waitForInput) == true) {
				_waitForInput = null;
				MayaSettings.tutorialPaused = false;
				MayaTime.slowMoEnabled = false;
				Destroy (_currentInstance.gameObject);
			}
		}
	}

	public void Show(GameObject prefab, string waitForInput) 
	{
		_currentInstance = Instantiate (prefab);
		MayaSettings.tutorialPaused = true;
		MayaTime.slowMoEnabled = true;
	}
}
