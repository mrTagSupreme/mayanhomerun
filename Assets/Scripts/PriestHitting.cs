﻿using UnityEngine;
using System.Collections;

public class PriestHitting : MonoBehaviour {
    public float ChargeTimeMaxHit = 1;
    public float OverchargeJitter = 0.05f;
    public float HitOverAngle = 45;
    public float HitSpeedIncFactor = 1.2f;

    private float radius;
    private Vector2 direction;
    private float chargingTime = 0;

	// Use this for initialization
	void Start () {
        radius = GetComponent<CircleCollider2D>().radius * transform.lossyScale.x;
        Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        direction = (mouseWorldPos - Vector2.zero).normalized;
        if (Input.GetButton("ChargeHit"))
        {
            chargingTime += Time.deltaTime;
            if (chargingTime > ChargeTimeMaxHit)
            {
                float jitterFactor = OverchargeJitter * Mathf.InverseLerp(ChargeTimeMaxHit, ChargeTimeMaxHit * 2.5f, chargingTime);
                direction.x += Random.Range(-jitterFactor, jitterFactor);
                direction.y += Random.Range(-jitterFactor, jitterFactor);
                direction.Normalize();
            }
        }
	}

    void FixedUpdate()
    {
        if (Input.GetButtonUp("ChargeHit"))
        {
            var colliderInHittingRange = Physics2D.OverlapCircleAll(transform.position, radius);
            foreach (var collider in colliderInHittingRange)
            {
                if (collider.tag != "Player")
                {
                    var collidingRigidBody = collider.GetComponent<Rigidbody2D>();
                    if (collidingRigidBody != null && !collidingRigidBody.isKinematic)
                    {
                        Vector2 dirToColliding = collidingRigidBody.position - (Vector2)transform.position;
                        float distToColliding = dirToColliding.magnitude;
                        dirToColliding /= distToColliding;
                        if (Mathf.Abs(Vector2.Angle(dirToColliding, direction)) < HitOverAngle / 2.0f)
                        {
                            float velocityMagnitude = collidingRigidBody.velocity.magnitude;
                            Vector2 newVelocity = collidingRigidBody.velocity / velocityMagnitude;
                            if(Vector2.Dot(newVelocity, direction) < 0)
                                newVelocity = Vector2.Reflect(newVelocity, direction);
                            newVelocity = Vector2.Lerp(newVelocity, direction, 1.0f);
                            float velocityFactor = HitSpeedIncFactor * (0.5f + 0.5f * Mathf.InverseLerp(radius, radius / 1.5f, distToColliding));
                            Debug.Log("velocityFactor: " + velocityFactor.ToString());
                            velocityMagnitude = Mathf.Max(velocityMagnitude, velocityFactor * 20.0f);
                            newVelocity *= velocityMagnitude * velocityFactor;
                            collidingRigidBody.velocity = newVelocity;
							collidingRigidBody.gravityScale = 1f;
                        }
                    }
                }
            }
            chargingTime = 0;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + (Vector3)direction * 5);
    }
}
