﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text MainScoreText;
    public GameObject HomerunCounterParent;
    public Text FinalScoreText;
	public Text ComboScoreText;

    private Text HomerunScoreText;

    bool firstHit = true;
    HackyHitSide lastHitSide;
    HackyHitCategory lastHitCategory;
    float lastHitTime;

    float currentComboscore = 0;
    int currentScore = 0;

	protected ComboParticleSpawner _comboParticleSpawner;

	// Use this for initialization
	void Start () {
        HomerunScoreText = HomerunCounterParent.transform.FindChild("homerunscore").GetComponent<Text>();
		_comboParticleSpawner = GetComponent<ComboParticleSpawner> ();
        HackyController.OnHackyHit += HackyController_OnHackyHit;
        Manager.OnHomerunStarted += Manager_OnHomerunStarted;
        Manager.OnHomerunFinished += Manager_OnHomerunFinished;
        Manager.OnHomerunning += Manager_OnHomerunning;
        Manager.OnGameFinished += Manager_OnGameFinished;
        FlyingHead.OnHeadHitGround += FlyingHead_OnHeadHitGround;
        EnemySpawner.OnNewEnemySpawned += EnemySpawner_OnNewEnemySpawned;
	}

    void EnemySpawner_OnNewEnemySpawned()
    {
        firstHit = true;
    }

    void Manager_OnGameFinished()
    {
        FinalScoreText.text = "Your score: " + currentScore.ToString();
    }

    void FlyingHead_OnHeadHitGround(FlyingHead head)
    {
        firstHit = true;
		ComboScoreText.text = "0";
    }

    void Manager_OnHomerunning(float currentDistance)
    {
        int homerunMultiplier = 1 + Mathf.FloorToInt( currentDistance / 10.0f);
        float newComboscore = currentComboscore * homerunMultiplier;
        HomerunScoreText.text = "Distance: " + ((int)currentDistance).ToString() + " Multiplier: " + homerunMultiplier.ToString() + " Comboscore: " + ((int)newComboscore).ToString();
		ComboScoreText.text = ((int)newComboscore).ToString();
    }

    void Manager_OnHomerunFinished(float distance)
    {
        int homerunMultiplier = 1 + Mathf.FloorToInt(distance / 10.0f);
        float newComboscore = currentComboscore * homerunMultiplier;
        currentScore += (int)newComboscore;
		MainScoreText.text = currentScore.ToString();
        HomerunCounterParent.SetActive(false);
    }

    void Manager_OnHomerunStarted(FlyingHead head)
    {
        HomerunScoreText.text = "";
        HomerunCounterParent.SetActive(true);
    }

	void HackyController_OnHackyHit(HackyHitCategory category, HackyHitSide side, Vector3 position)
    {
		if (!firstHit) {
			switch (category) {
			case HackyHitCategory.Head:
				currentComboscore += 5;
				break;
			case HackyHitCategory.Arms:
				currentComboscore += 10;
				break;
			case HackyHitCategory.Feet:
				currentComboscore += 30;
				break;
			}
			if ((lastHitSide == HackyHitSide.Left && side == HackyHitSide.Right) ||
			             (lastHitSide == HackyHitSide.Right && side == HackyHitSide.Left)) {
				currentComboscore += 10;
			}
			if (lastHitSide != side) {
				currentComboscore += 5;
			}

			if ((lastHitCategory == HackyHitCategory.Arms && category == HackyHitCategory.Feet) ||
			             (lastHitCategory == HackyHitCategory.Feet && category == HackyHitCategory.Arms)) {
				currentComboscore += 15;
			} else if ((lastHitCategory == HackyHitCategory.Arms && category == HackyHitCategory.Head) ||
			                 (lastHitCategory == HackyHitCategory.Head && category == HackyHitCategory.Arms)) {
				currentComboscore += 5;
			} else if ((lastHitCategory == HackyHitCategory.Feet && category == HackyHitCategory.Head) ||
			                  (lastHitCategory == HackyHitCategory.Head && category == HackyHitCategory.Feet)) {
				currentComboscore += 10;
			}

			float airTime = Time.realtimeSinceStartup - lastHitTime;
			airTime = Mathf.Round (airTime);
			currentComboscore *= Mathf.Max (airTime, 1.0f);

			Debug.Log ("comboscore: " + currentComboscore.ToString () + "category: " + category.ToString () + " side: " + side.ToString () + " lastCatgory: " + lastHitCategory.ToString () + " lastSide: " + lastHitSide.ToString () + " airTime: " + airTime.ToString ());
			ComboScoreText.text = (currentComboscore).ToString ();

			// fake prozent berechnung
			SoundManager.instance.Play (SoundManager.instance.combos, currentComboscore / 100f);

			_comboParticleSpawner.SpawnCombo ((int)currentComboscore, position);
		} else {
			currentComboscore = 0f;
		}
        lastHitCategory = category;
        lastHitSide = side;
        lastHitTime = Time.realtimeSinceStartup;
        firstHit = false;
    }

}
