﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct ArrowProps
{
	public Vector2 screenPos;
	public FlyingHead head;
}

public class ArrowHUD : MonoBehaviour {

	public GameObject arrowPrefab;

	protected List<ArrowProps> _currentHeads = new List<ArrowProps>();
	protected CameraFollow _cameraFollow;
	protected Camera _camera;

	protected List<ArrowItem> _currentArrows = new List<ArrowItem>();



	void Start () {
		_cameraFollow = GameObject.FindObjectOfType<CameraFollow> ();
		_camera = _cameraFollow.GetComponent<Camera> ();
	}
	

	protected void Update ()
	{
		if (_camera != null && arrowPrefab != null) {
			_currentHeads.Clear ();

			for (int i = 0; i < FlyingHead.FlyingHeads.Count; i++) {
				FlyingHead head = FlyingHead.FlyingHeads [i];
				Vector2 screenPos =	_camera.WorldToViewportPoint (head.transform.position);

				if (screenPos.y > 1f) {
					ArrowProps item = new ArrowProps ();
					item.head = head;
					item.screenPos = screenPos;
					_currentHeads.Add (item);
				}
			}
		}
	}

	protected void LateUpdate()
	{
		Vector3 posAtTop = _camera.ViewportToWorldPoint (new Vector3 (0f, 0f, _camera.transform.position.z));

		if (arrowPrefab != null) {
			// pooling: create (just increase, dont decrease)
			for (int i = _currentArrows.Count; i < _currentHeads.Count; i++) {
				GameObject item = Instantiate<GameObject> (arrowPrefab);
				item.transform.SetParent(this.transform);
				_currentArrows.Add (item.GetComponent<ArrowItem>());
			}

			for (int i = 0; i < _currentHeads.Count; i++) {
				_currentArrows [i].transform.position = new Vector3(_currentHeads [i].screenPos.x * Screen.width, Screen.height, 0f);
				_currentArrows [i].UpdateText (_currentHeads [i].head.transform.position.y - posAtTop.y);
				_currentArrows [i].gameObject.SetActive (true);
			}

			// pooling: disable rest
			for (int i = _currentHeads.Count; i < _currentArrows.Count; i++) {
				_currentArrows [i].gameObject.SetActive (false);
			}
		}
	}
}
