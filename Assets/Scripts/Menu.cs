﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MenuTransition
{
	public float sunTime = 15f;
	public float atmosphereThickness = 1.12f;
}

public class Menu : MonoBehaviour {

	public MenuTransition from;
	public MenuTransition to;

	public GameObject HUDInGame;
	public GameObject HUDMainMenu;
	public GameObject HUDCredits;

	protected bool _open = false;

	protected CameraFollow _cameraFollow;
	protected GameObject _player;
	protected Sun _sun;

    public delegate void StartGameDelegate();
    public event StartGameDelegate OnStartGame = delegate { };


	protected float _velocity;
	protected float _percent;

	protected bool _showCredits = false;

	void Awake () {
		_cameraFollow = GameObject.FindObjectOfType<CameraFollow> ();
		_player = GameObject.FindGameObjectWithTag ("PlayerFollowPivot");
		_sun = GameObject.FindObjectOfType<Sun> ();
	}


	void Update () {
		if (enabled == true) {

			if (_cameraFollow != null) {

				_percent = Mathf.SmoothDamp (_percent, _open == true ? 0f : 1f, ref _velocity, _cameraFollow.followSpeed);

				if (_sun != null)
					_sun.time = Mathf.Lerp (from.sunTime, to.sunTime, _percent);

				if (RenderSettings.skybox != null)
					RenderSettings.skybox.SetFloat("_AtmosphereThickness", Mathf.Lerp (from.atmosphereThickness, to.atmosphereThickness, _percent));
			}
		}
	}

	public bool open {
		get { return _open; }
		set { 
			if (_open != value && _cameraFollow != null && _player != null) {
				_cameraFollow.target = value == true ? transform : _player.transform;

				HUDInGame.SetActive (value == false);
				HUDMainMenu.SetActive (value == true);
			}
			_open = value;
		}
	}

	public bool showCredits
	{
		get { return _showCredits; }
		set {

			open = value;

			if (value == true) {
				
				HUDMainMenu.SetActive (false);
				HUDCredits.SetActive (true);
			} else
				open = _open;
			
			_showCredits = value;
		}
	}
}
