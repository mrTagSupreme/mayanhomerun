﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public EnemyPath path;

	public float direction = -1f;

	public bool walk = true;

	public float walkSpeed = 0.1f;

	public float _progress = 0f;

	public bool waitInQueue = true;

	public float queuePercentage = 0f; // will be filled out by EnemySpawner

	public EnemySpawner spawner; // will be filled out by Enemy Spawner

	public FlyingHead head;

	protected Animator _animator;

	protected bool _waitForEndIdle = false;

	protected bool _spawnNextTriggered = false;

	void Awake() {
		// damit er nicht am anfang im bild steht
		transform.position = Vector3.right * 10000;
	}

	void Start () 
	{
		_animator = GetComponentInChildren<Animator> ();

		if (direction < 0f) {
			_progress = 1f;
			transform.localScale = new Vector3(-1f, 1f, 1f);
		}

		Update ();
	}

	public void Decapitate()
	{
		if (_animator != null) {
			_waitForEndIdle = true;
			_animator.SetTrigger ("Decapitate");
			SoundManager.instance.Play (SoundManager.instance.decapitate);
			SoundManager.instance.Play (SoundManager.instance.headoff);
		}
	}

	void Update () 
	{
		if (enabled == true) {
			bool walkAllowed = true;

			if (_animator != null && _waitForEndIdle == true) {
				AnimatorClipInfo[] info = _animator.GetCurrentAnimatorClipInfo (0);
				if (info != null && info.Length > 0) {
					walkAllowed = (info [0].clip.name == "Walk");
					_waitForEndIdle = false;
				}
			}

			float oldProgress = _progress;

			if (enabled == true && MayaSettings.enemyMovementEnabled == true) {
				if (walk == true && walkAllowed == true) {
					_progress += direction * walkSpeed * MayaTime.deltaTime;

					float min = 0f;
					float max = 1f;

					// queueing...
					if (waitInQueue == true) {
						if (direction < 0)
							min = queuePercentage;
						else
							max = queuePercentage;
					}

					_progress = Mathf.Min (max, Mathf.Max (min, _progress)); // range 0 - 1

					// check for destroy
					if ((direction > 0f && _progress >= 1f) || 
						(direction < 0f && _progress <= 0f))
						Destroy (gameObject);
				}

				if (path != null)
					this.transform.position = path.GetVectorByPercent (_progress);
			}

			_animator.SetBool ("Idle", oldProgress == _progress);


			// spawn next, if this one has passed with head on
			if (_spawnNextTriggered == false && head != null && head.transform.parent != null) {
				
				if ((direction > 0f && _progress > spawner.enemyPath.waitPercentRight + 0.1f) ||
					(direction < 0f && _progress < spawner.enemyPath.waitPercentLeft - 0.1f))
				{
					spawner.SpawnNext ();
					_spawnNextTriggered = true;
				}
			}
		}
	}
}
