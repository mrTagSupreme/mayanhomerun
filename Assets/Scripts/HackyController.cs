﻿using UnityEngine;
using System.Collections;

enum InputType
{
    None,
    Head,
    Upper,
    Lower,
	Decapitate,
    ChargeSword,
    HittingSword
}

public enum HackyHitCategory 
{
    Head,
    Arms,
    Feet
}

public enum HackyHitSide 
{
    Left,
    Middle,
    Right
}

public class HackyController : MonoBehaviour {

    public float ArmsHorizontalForce = 3.0f;
    public float ArmsVerticalFactor = 1.02f;
    public float FeetHorizontalForce = 2.0f;
    public float FeetVerticalFactor = 1.2f;
    public float HeadHorizontalForce = -2.0f;
    public float HeadVerticalFactor = 1.0f;
    public float DecapitateVelocity = 15;
    public float InputActiveTime = 0.25f;
    public float SwordSwingTime = 1.0f;
    public float SwordAddVelocity = 20;
    public Vector2 SwordSwingStart;
    public Vector2 SwordSwingEnd;

    BoxCollider2D headColl;
    BoxCollider2D armLColl;
    BoxCollider2D armRColl;
    BoxCollider2D footLColl;
    BoxCollider2D footRColl;
	CircleCollider2D sword;
    Transform visualPriest;

    InputType activeInput = InputType.None;
    HackyHitSide activeInputSide;
    float remainingTimeActive;
    float currentInputTime;

    bool swordIsSwinging = false;
    float swordSwingPercent = 0;
    bool invertSwing = false;
    Vector2 swingVectorInverter = new Vector2(-1, 1);
    Vector2 swingDir;
    float swingDelay;

	public delegate void HackyHitDelegate(HackyHitCategory category, HackyHitSide side, Vector3 position);
    public static event HackyHitDelegate OnHackyHit = delegate { };

    public delegate void SwordModeTriggered();
    public static event SwordModeTriggered OnSwordModeTriggered = delegate { };


	protected Animator _animator;
	protected CameraFollow _cameraFollow;

	// Use this for initialization
	void Start () {
        headColl = transform.FindChild("Head").GetComponent<BoxCollider2D>();
        armLColl = transform.FindChild("ArmL").GetComponent<BoxCollider2D>();
        armRColl = transform.FindChild("ArmR").GetComponent<BoxCollider2D>();
        footLColl = transform.FindChild("FootL").GetComponent<BoxCollider2D>();
        footRColl = transform.FindChild("FootR").GetComponent<BoxCollider2D>();
		sword = transform.FindChild("Sword").GetComponent<CircleCollider2D>();

        visualPriest = transform.FindChild("priest");

		_animator = GetComponentInChildren<Animator> ();
		_cameraFollow = GameObject.FindObjectOfType<CameraFollow> ();
	}

    // Update is called once per frame
    void Update()
    {
        if (activeInput == InputType.None)
        {
            if (Input.GetButtonDown("UpperHit"))
                TriggerInput(HackyHitCategory.Arms);
            else if (Input.GetButtonDown("LowerHit"))
                TriggerInput(HackyHitCategory.Feet);
            else if (Input.GetButtonDown("Head"))
                TriggerInput(HackyHitCategory.Head);
            else if (Input.GetButtonDown("Decapitate"))
                TriggerSwordMode();
            remainingTimeActive = InputActiveTime;
            currentInputTime = 0;
        }
		FlyingHead head;

        switch(activeInput){
		case InputType.Upper:
                if (currentInputTime > 0.1f && currentInputTime < 0.5f)
                {
                    if(activeInputSide == HackyHitSide.Left && CheckCollision(armLColl, ArmsHorizontalForce, ArmsVerticalFactor, out head))
                    {
                        OnHackyHit(HackyHitCategory.Arms, HackyHitSide.Left, head.transform.position);
                    }
                    else if(activeInputSide == HackyHitSide.Right && CheckCollision(armRColl, ArmsHorizontalForce, ArmsVerticalFactor, out head))
                    {
                        OnHackyHit(HackyHitCategory.Arms, HackyHitSide.Right, head.transform.position);
                    }
                }
                break;
            case InputType.Lower:
                if (currentInputTime > 0.1f && currentInputTime < 0.5f)
                {
                    if (activeInputSide == HackyHitSide.Left && CheckCollision(footLColl, FeetHorizontalForce, FeetVerticalFactor, out head))
                    {
                        OnHackyHit(HackyHitCategory.Feet, HackyHitSide.Left, head.transform.position);
                    }
                    else if (activeInputSide == HackyHitSide.Right && CheckCollision(footRColl, FeetHorizontalForce, FeetVerticalFactor, out head))
                    {
                        OnHackyHit(HackyHitCategory.Feet, HackyHitSide.Right, head.transform.position);
                    }
                }
                break;
            case InputType.Head:
                if (currentInputTime > 0.1f && currentInputTime < 0.5f)
                {
                    if (CheckCollision(headColl, HeadHorizontalForce, HeadVerticalFactor, out head))
                    {
                        OnHackyHit(HackyHitCategory.Head, HackyHitSide.Middle, head.transform.position);
                    }
                }
                break;
		case InputType.ChargeSword:
			if (swingDelay < 0) {
				if (SwordSwing ()) {

				} else if (swordSwingPercent > 1) {
					activeInput = InputType.None;
					swordSwingPercent = 0;
					swordIsSwinging = false;
                    visualPriest.localScale = Vector3.one;

					MayaTime.slowMoEnabled = false;
					_cameraFollow.fieldOfView = 30;


				}

			}
			swingDelay -= Time.deltaTime;
                break;
        }

        currentInputTime += Time.deltaTime;
        remainingTimeActive -= Time.deltaTime;
        if (remainingTimeActive < 0 && activeInput != InputType.ChargeSword)
        {
            activeInput = InputType.None;
        }
	}

    private void TriggerInput(HackyHitCategory hackyHitCategory)
    {
		SoundManager.instance.Play (SoundManager.instance.hits);

        if (hackyHitCategory == HackyHitCategory.Head)
        {
            _animator.SetTrigger("Head");
            activeInput = InputType.Head;
            activeInputSide = HackyHitSide.Middle;
        }
        else
        {
            foreach (var flyingHead in FlyingHead.FlyingHeads)
            {
                if (flyingHead.isFlying)
                {
                    if (flyingHead.transform.position.x < transform.position.x)
                        activeInputSide = HackyHitSide.Left;
                    else
                        activeInputSide = HackyHitSide.Right;
                }
            }
            if (hackyHitCategory == HackyHitCategory.Arms)
            {
                if (activeInputSide == HackyHitSide.Right) _animator.SetTrigger("ArmRight");
                else _animator.SetTrigger("ArmLeft");
                activeInput = InputType.Upper;
            }
            else
            {
                if (activeInputSide == HackyHitSide.Right) _animator.SetTrigger("FeetRight");
                else _animator.SetTrigger("FeetLeft");
                activeInput = InputType.Lower;
            }
        }
    }

	bool CheckCollision(BoxCollider2D collider, float horizontalForce, float verticalFactor, out FlyingHead head)
    {
		head = null;

        bool collided = false;
        foreach (var flyingHead in FlyingHead.FlyingHeads)
        {
            if (flyingHead.isFlying && Physics2D.IsTouching(flyingHead.coll, collider))
            {
                float xOffset = flyingHead.coll.transform.position.x - transform.position.x;
                float xForceFactor = -Mathf.Sign(xOffset) * Mathf.InverseLerp(0, 6, Mathf.Abs(xOffset));
				if (flyingHead.body != null) {
                    Vector2 newVelocity = flyingHead.body.velocity;
                    newVelocity.y = Mathf.Abs(flyingHead.body.velocity.y) * verticalFactor;
					newVelocity.x += xForceFactor * horizontalForce;
                    flyingHead.body.velocity = newVelocity;
					activeInput = InputType.None;
					collided = true;

                    flyingHead.body.AddTorque(xForceFactor * 10000.0f);

					head = flyingHead;
					break;
				}
            }
        }
        return collided;
    }

    void TriggerSwordMode()
    {
        bool decapitated = false;
        bool flying = false;
        foreach (var flyingHead in FlyingHead.FlyingHeads)
        {
            if (!flyingHead.isFlying)
            {
                float distToHead = Mathf.Abs(flyingHead.transform.position.x - transform.position.x);
                if (distToHead < 1)
                {
                    flyingHead.isFlying = true;
                    flyingHead.body = flyingHead.gameObject.AddComponent<Rigidbody2D>();
                    flyingHead.body.velocity = Vector2.up * DecapitateVelocity;
                    flyingHead.body.interpolation = RigidbodyInterpolation2D.Interpolate;
                    flyingHead.transform.SetParent(null);
                    activeInput = InputType.None;
                    decapitated = true;
                    Enemy enemy = flyingHead.enemy;
					if (enemy != null)
						enemy.Decapitate ();
                }
            }
            else
            {
                flying = true;
            }
        }

        if (!flying)
        {
            _animator.SetTrigger("Decapitate");
        }

        if (!decapitated && flying)
        {
            activeInput = InputType.ChargeSword;
            swordIsSwinging = false;
            swordSwingPercent = 0;
            swingDelay = 0.1f;
            foreach (var flyingHead in FlyingHead.FlyingHeads)
            {
                if (flyingHead.isFlying)
                    invertSwing = flyingHead.transform.position.x < transform.position.x;
            }
            swingDir = SwordSwingEnd - SwordSwingStart;
            swingDir.Normalize();
            if (invertSwing)
                swingDir.Scale(swingVectorInverter);

			MayaTime.slowMoEnabled = true;
			_cameraFollow.fieldOfView = 15;
            if (!invertSwing)
                visualPriest.localScale = new Vector3(-visualPriest.localScale.x, 1, 1);
            _animator.SetTrigger("BatAusholer");
        }
    }

	bool SwordSwing ()
	{
        if (!swordIsSwinging)
        {
            if (!Input.GetButton("Decapitate"))
            {
                swordIsSwinging = true;
                _animator.SetTrigger("BatSchwinger");
            }
        }
        else
        {
            bool collided = false;
            foreach (var flyingHead in FlyingHead.FlyingHeads)
            {
                if (Physics2D.IsTouching(flyingHead.coll, sword))
                {
                    // decapitate
                    //Rigidbody2D flyingHeadRigidBody = flyingHeadCollider.GetComponent<Rigidbody2D>();
                    //if (flyingHeadRigidBody == null)
                    //{
                    //    flyingHeadRigidBody = flyingHeadCollider.gameObject.AddComponent<Rigidbody2D>();
                    //    flyingHeadRigidBody.AddForce(Vector2.up * verticalForce);
                    //    activeInput = InputType.None;
                    //    collided = true;
                    //}
                    if (flyingHead.isFlying)
                    {
                        Vector2 dir2Head = flyingHead.transform.position - sword.transform.position;
                        dir2Head.Normalize();
                        float inSwingDir;
                        inSwingDir = Vector2.Dot(dir2Head, swingDir);
                        Debug.Log("inSwingDir: " + inSwingDir.ToString());
                        if (inSwingDir > 0)
                        {
                            Vector2 newVelocity = inSwingDir * swingDir * SwordAddVelocity;
                            flyingHead.body.velocity = newVelocity;
                            flyingHead.body.angularVelocity = (1 - inSwingDir) * 3;

							SoundManager.instance.Play (SoundManager.instance.homerun);

                            return true;
                        }
                    }
                }
            }

            swordSwingPercent += (1.0f / SwordSwingTime) * Time.deltaTime;

            if (!invertSwing)
                sword.transform.localPosition = Vector2.Lerp(SwordSwingStart, SwordSwingEnd, swordSwingPercent);
            else
                sword.transform.localPosition = Vector2.Lerp(Vector2.Scale(SwordSwingStart, swingVectorInverter), Vector2.Scale(SwordSwingEnd, swingVectorInverter), swordSwingPercent);

            return collided;
        }
        return false;
	}
}
