﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlyingHead : MonoBehaviour {

    public static List<FlyingHead> FlyingHeads = new List<FlyingHead>();

    [HideInInspector]
    public Rigidbody2D body = null;
    
    [HideInInspector]
    public bool homerun = false;
    [HideInInspector]
    public Collider2D coll = null;
	public Enemy enemy;
    public ParticleSystem splash;

    private float MaxVelocity = 15;

    public delegate void HeadHitGround(FlyingHead head);
    public static event HeadHitGround OnHeadHitGround = delegate { };

    bool _isFlying = false;
    public bool isFlying
    {
        get { return _isFlying; }
        set
        {
            if (value)
            {
                transform.FindChild("Trail").gameObject.SetActive(true);
            }
            _isFlying = value;
        }
    }

	// Use this for initialization
	void Start () {
        coll = GetComponent<Collider2D>();
        FlyingHeads.Add(this);
	}

	void Update() {
        if (transform.position.y < -15f)
        {
            OnHeadHitGround(this);
            Destroy(gameObject);

			// spawn next, if decapitated and below ground.
			EnemySpawner spawner = GameObject.FindObjectOfType<EnemySpawner>();
			if (spawner != null)
				spawner.SpawnNext ();
        }
        if (!homerun && isFlying && body != null)
        {
            if (body.velocity.y < 0)
            {
                float bodyVelocity = body.velocity.magnitude;
                if (bodyVelocity > MaxVelocity)
                {
                    Vector2 newVelocity = body.velocity / bodyVelocity;
                    newVelocity *= MaxVelocity;
                    body.velocity = newVelocity;
                }
            }
        }
	}

    void OnTriggerExit2D(Collider2D coll)
    {
        OnTriggerEnter2D(coll);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Finish" && _isFlying)
        {
            var splashInstance = Instantiate<ParticleSystem>(splash);
            splashInstance.transform.position = transform.position;
            Destroy(splashInstance.gameObject, 5);

            isFlying = false;

            OnHeadHitGround(this);
            Destroy(gameObject);
            EnemySpawner spawner = GameObject.FindObjectOfType<EnemySpawner>();
            if (spawner != null)
                spawner.SpawnNext();
        }
    }

    void OnDestroy()
    {
        FlyingHeads.Remove(this);
    }

	public static void Reset()
	{
		FlyingHeads = new List<FlyingHead>();
	}

}
