﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Sun : MonoBehaviour 
{
	[Range(0f, 24f)]
	public float time = 12f;

	public float speed = 1f;

	public Transform pivot;
	public float radius = 10f;

	public float offset = 0f;

	protected float _lastTime = -1f;

	void Start () 
	{
		transform.hideFlags = HideFlags.NotEditable;
	}

	public void Update () 
	{
		time = time < 0f ? 24f : time > 24f ? 0f : time;

		if (_lastTime != time)
		{
			float percent = (time - 6f) / 24f;

			Vector3 newPos = Vector3.zero;

			float offsetPercent = -percent + offset;

			newPos.x = Mathf.Cos(offsetPercent * 2 * Mathf.PI) * radius;
			newPos.y = Mathf.Sin(percent * 2 * Mathf.PI) * radius;
			newPos.z = Mathf.Sin(offsetPercent  * 2 * Mathf.PI) * radius;

			transform.position = newPos;
			transform.LookAt(pivot);

			_lastTime = time;
		}

		time += speed * Time.deltaTime;
	}
}