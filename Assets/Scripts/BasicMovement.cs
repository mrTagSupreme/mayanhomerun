﻿using UnityEngine;
using System.Collections;

public class BasicMovement : MonoBehaviour {

	public float speed = 0.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float delta = Input.GetAxis ("Horizontal") * speed;

		Vector3 pos = this.transform.position;
		pos.x += delta;
		this.transform.position = pos;

		if (delta > float.Epsilon || delta < -float.Epsilon)
			this.transform.localScale = new Vector3(Mathf.Sign(delta), 1f, 1f);
	}
}
