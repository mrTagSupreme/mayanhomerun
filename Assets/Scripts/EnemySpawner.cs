﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {

	public bool spawnLeft = false;

	public bool alternate = true;

	public float spawnTime = 1f;

	protected float queuePadding = 0.03f;

	public int maxQueued = 10;

	public GameObject spawnPrefab;

	protected bool _spawnNextAutomatically = false;

	protected float _timer = 0f;
	protected EnemyPath _enemyPath;

	protected List<Enemy> _queue = new List<Enemy>();

	public int totalSpawned = 0;

	public int totalStarted = 0;

    public delegate void NewEnemySpawned();
    public static event NewEnemySpawned OnNewEnemySpawned = delegate { };

	void Start () 
	{
		_enemyPath = GameObject.FindObjectOfType<EnemyPath> ();
	}

	void Update () 
	{
		if (enabled == true) {
			_timer += MayaTime.deltaTime;

			if (_timer > spawnTime) {

				// only spawn up to 10 enemies at all.
				if (MayaSettings.instance.maxEnemies <= 0 ||
				    totalSpawned < MayaSettings.instance.maxEnemies) {
					if (_queue.Count <= maxQueued) {
						if (spawnPrefab != null) {
							GameObject obj = Instantiate (spawnPrefab);
							Enemy enemy = obj.GetComponent<Enemy> ();
							enemy.direction = (spawnLeft ? 1f : -1f);
							enemy.path = _enemyPath;
							enemy.spawner = this;
							_queue.Add (enemy);
							ReadjustQueuedPositions ();

							totalSpawned++;
						}

						if (alternate == true)
							spawnLeft = !spawnLeft;
					}
				}
				_timer = 0f;
			}

			// spawn next, if requested
			if (_spawnNextAutomatically == true && _queue.Count > 0) {
				Enemy enemy = _queue[0];
				_queue.RemoveAt (0);
				enemy.waitInQueue = false;

				ReadjustQueuedPositions ();
				_spawnNextAutomatically = false;

				SoundManager.instance.Play (SoundManager.instance.demon);

                OnNewEnemySpawned();

				totalStarted++;
			}
		}
	}

	public void SpawnNext()
	{
		_spawnNextAutomatically = true;

	}

	protected void ReadjustQueuedPositions()
	{
		for (int i = 0; i < _queue.Count; i++) {
			Enemy enemy = _queue [i];

			//float index = alternate == false ? i : Mathf.Floor(i / 2);
			float padding = i * queuePadding;


			enemy.queuePercentage = (enemy.direction > 0f ? _enemyPath.waitPercentLeft - padding : _enemyPath.waitPercentRight + padding);
		}
	}

	public EnemyPath enemyPath
	{
		get { return _enemyPath; }
	}
}
