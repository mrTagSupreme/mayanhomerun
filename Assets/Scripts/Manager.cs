﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using UnityEngine.SceneManagement;

enum GameStates
{
    None,
    PressStart,
    Ingame,
	Credits
}

enum IngameState
{
    HackySacky,
    Homerun
}

public class Manager : MonoBehaviour {

    public Menu MenuObject;
    public AudioSource IngameMusic;
    public AudioMixerSnapshot IngameNormalSnapshot;
    public AudioMixerSnapshot MenuSnapshot;

    private GameStates currentState = GameStates.None;
    private IngameState currentIngameState = IngameState.HackySacky;
    private FlyingHead currentHomerunHead = null;
    private float homerunDistance = 0;

    protected CameraFollow _cameraFollow;
    protected GameObject _player;

	protected EnemySpawner _spawner;

    public delegate void HomerunStarted(FlyingHead head);
    public static event HomerunStarted OnHomerunStarted = delegate { };

    public delegate void Homerunning(float currentDistance);
    public static event Homerunning OnHomerunning = delegate { };

    public delegate void HomerunFinishedDelegate(float distance);
    public static event HomerunFinishedDelegate OnHomerunFinished = delegate { };

    public delegate void GameFinished();
    public static event GameFinished OnGameFinished = delegate { };
	protected float endGameTimer = 1.5f;


	void Start () 
	{
        _cameraFollow = GameObject.FindObjectOfType<CameraFollow>();
        _player = GameObject.FindGameObjectWithTag("PlayerFollowPivot");
		_spawner = GameObject.FindObjectOfType<EnemySpawner> ();

		FlyingHead.Reset ();
        switchState(GameStates.PressStart);

		GameObject.FindObjectOfType<EnemySpawner> ().SpawnNext ();
        FlyingHead.OnHeadHitGround += FlyingHead_OnHeadHitGround;
	}

    void FlyingHead_OnHeadHitGround(FlyingHead head)
    {
        if (currentState == GameStates.Ingame && currentIngameState == IngameState.Homerun)
        {
            homerunDistance = Mathf.Abs(currentHomerunHead.transform.position.x);
            Invoke("homerunFinished", 1.5f);
        }
        
        if (head == currentHomerunHead)
        {
            currentHomerunHead = null;
        }
    }

    void homerunFinished()
    {
        switchIngameState(IngameState.HackySacky);
        OnHomerunFinished(homerunDistance);
    }

	void checkEndGame()
    {
        // end of game
		if (currentState != GameStates.Credits  && _spawner.totalStarted == MayaSettings.instance.maxEnemies && FlyingHead.FlyingHeads.Count == 0)
        {
			if (endGameTimer > 0f)	endGameTimer -= Time.deltaTime;
			else 		           	switchState(GameStates.Credits);
        }			
    }

	void Update()
	{
		if (currentState != GameStates.Credits) {
			if (MenuObject.open == true && Input.anyKeyDown == true) {
				switchState (GameStates.Ingame);
				SoundManager.instance.Play (SoundManager.instance.pressAnyKey);
			} else if (MenuObject.open == false && Input.GetButtonDown ("Menu"))
				switchState (GameStates.PressStart);

		}

		if (currentState == GameStates.Credits && Input.anyKeyDown == true) {
			SceneManager.LoadScene (0);
		}

        if (currentState == GameStates.Ingame)
        {
            if (currentIngameState == IngameState.HackySacky)
            {
                foreach (var head in FlyingHead.FlyingHeads)
                {
                    if (head.isFlying && Mathf.Abs(head.transform.position.x) > 10)
                    {
                        currentHomerunHead = head;
                        currentHomerunHead.homerun = true;
                        switchIngameState(IngameState.Homerun);
                        OnHomerunStarted(currentHomerunHead);
                        break;
                    }
                }
            }
            else
            {
                if (currentHomerunHead != null)
                {
                    homerunDistance = currentHomerunHead.transform.position.x;
                    OnHomerunning(Mathf.Abs(currentHomerunHead.transform.position.x));
                }
            }


			checkEndGame ();
        }
	}

    void switchState(GameStates newState)
    {
        switch (newState)
        {
		case GameStates.PressStart:
				MenuObject.open = true;
				MenuSnapshot.TransitionTo (0.5f);
				MayaSettings.paused = true;
                break;
            case GameStates.Ingame:
                IngameMusic.Play();
                MenuObject.open = false;
                IngameNormalSnapshot.TransitionTo(0.5f);
                switchIngameState(IngameState.HackySacky);
				MayaSettings.paused = false;
                break;
			case GameStates.Credits:
				MayaSettings.paused = true;
				MenuObject.showCredits = true;
                OnGameFinished();
				break;
        }

		currentState = newState;
    }

    void switchIngameState(IngameState newIngameState)
    {
        switch (newIngameState)
        {
            case IngameState.HackySacky:
                _cameraFollow.target = _player.transform;
                _cameraFollow.fieldOfView = 30;
                _cameraFollow.followSpeed = 0.5f;
                break;
            case IngameState.Homerun:
                _cameraFollow.target = currentHomerunHead.transform;
                _cameraFollow.fieldOfView = 40;
                _cameraFollow.followSpeed = 0.1f;
                break;
        }
        currentIngameState = newIngameState;
    }
}
