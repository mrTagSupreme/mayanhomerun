﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public Transform target;

	public float zDistance = -20f;

	public float followSpeed = 0.5f;

	public float fieldOfView = 30f;

	protected Vector3 _velocity = Vector3.zero;
	protected Vector3 _rotVelocity = Vector3.zero;

	protected float _followSpeedVelocity = 0f;
	protected float _currentFollowSpeed = 0f;

	protected float _fieldOfViewVelocity = 0f;

	protected Menu _menu;
	protected Camera _camera;

	void Start () 
	{
		_menu = GameObject.FindObjectOfType<Menu> ();
		_camera = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		_currentFollowSpeed = Mathf.SmoothDamp (_currentFollowSpeed, followSpeed, ref _followSpeedVelocity, 1f);
		if (target != null) {
			Vector3 newPosition = new Vector3 (target.transform.position.x,
				                     target.transform.position.y,
				                     target.transform.position.z + zDistance);

			Vector3 newEuler = target.transform.rotation.eulerAngles;

			Vector3 pos = Vector3.SmoothDamp (transform.position, newPosition, ref _velocity, followSpeed);
			Vector3 rot = new Vector3 (Mathf.SmoothDampAngle (transform.rotation.eulerAngles.x, newEuler.x, ref _rotVelocity.x, followSpeed),
				             Mathf.SmoothDampAngle (transform.rotation.eulerAngles.y, newEuler.y, ref _rotVelocity.y, followSpeed),
				             Mathf.SmoothDampAngle (transform.rotation.eulerAngles.z, newEuler.z, ref _rotVelocity.z, followSpeed));

			if (_camera != null)
				_camera.fieldOfView = Mathf.SmoothDamp (_camera.fieldOfView, fieldOfView, ref _fieldOfViewVelocity, followSpeed);

			this.transform.position = pos;
			//this.transform.rotation = Quaternion.Euler (rot);
		}
	}
}
