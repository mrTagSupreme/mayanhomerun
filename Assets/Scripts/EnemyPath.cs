﻿using UnityEngine;
using System.Collections;

public class EnemyPath : MonoBehaviour {

	public Transform[] points;

	public Transform waitPointLeft;
	public Transform waitPointRight;


	protected float[] lengths;
	protected float totalLength;

	public float _waitPercentLeft;
	public float _waitPercentRight;

	void Start () 
	{
		// get length
		lengths = new float[points.Length];
		totalLength = 0f;

		for (int i = 1; i < points.Length; i++) {
			float length = Vector3.Distance(points[i-1].position, points[i].position);
			lengths[i] = length;
			totalLength += length;
		}

		_waitPercentLeft = GetPercentByPoint (waitPointLeft);
		_waitPercentRight = GetPercentByPoint (waitPointRight);
	}
	

	void Update () 
	{
	
	}

	void OnDrawGizmos()
	{
		if (points.Length > 1) {
			Gizmos.color = Color.green;

			for (int i = 0; i < points.Length - 1; i++) {
				if (points [i] != null && points [i + 1] != null)
					Gizmos.DrawLine (points [i].position, points [i + 1].position);
			}
		}
	}

	public float GetPercentByPoint(Transform point)
	{
		float percent = 0f;

		if (points.Length > 1 && point != null) {
			float totalPointLength = 0f;
			for (int i = 1; i < points.Length; i++) {
				totalPointLength += lengths[i];

				if (points [i] == point) {
					percent = totalPointLength / totalLength;
					break;
				}
			}
		}
		return percent;

	}

	public Vector3 GetVectorByPercent(float percent)
	{
		Vector3 point = Vector3.zero;

		if (points.Length > 1)
		{
			float findLength = totalLength * percent;
			float startLength = 0;

			for (int i = 1; i < lengths.Length; i++) {
				if (findLength >= startLength && findLength < startLength + lengths [i]) {
					float partLength = findLength - startLength;
					float partPercentage = partLength / lengths [i];

					point = Vector3.Lerp (points [i-1].position, points [i].position, partPercentage);
					break;
				}

				startLength += lengths [i];
			}
		}
		return point;
	}

	public float waitPercentRight
	{
		get { return _waitPercentRight; }
	}

	public float waitPercentLeft
	{
		get { return _waitPercentLeft; }
	}
}
