﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class MayaTime : MonoBehaviour {
	
    public AudioMixerSnapshot NormalTimeSnapshot;
    public AudioMixerSnapshot SlowmoSnapshot;

	public float slowMo = 0.2f;

	protected static MayaTime _instance;

	protected bool _slowMoEnabled = false;

	protected float _deltaTime;
	protected float _lastTimeSinceLevelLoad;

	protected float _slowMoVelocity = 0f;

	void Awake() 
	{
		_instance = this;
	}

	void OnEnable()
	{
		_instance = this;
	}

	void Start () 
	{
	
	}

	void Update () 
	{
		_deltaTime = Time.timeSinceLevelLoad - _lastTimeSinceLevelLoad;
		_lastTimeSinceLevelLoad = Time.timeSinceLevelLoad;

		#if UNITY_EDITOR
		if (Input.GetButtonDown("SlowMo"))
			_slowMoEnabled = !_slowMoEnabled;
		#endif

		Time.timeScale = Mathf.SmoothDamp(Time.timeScale, (_slowMoEnabled == true ? slowMo : 1f), ref _slowMoVelocity, 0.1f);
	}


	#if UNITY_EDITOR
	void OnGUI()
	{
		if (_slowMoEnabled == true)
			GUI.TextField (new Rect (0, 0, 60, 20), "slow mo");
	}
	#endif

	public static float deltaTime {
		get { return _instance._deltaTime; }
	}

	public static bool slowMoEnabled {
		get { return _instance._slowMoEnabled; }
        set
        {
            _instance._slowMoEnabled = value;
            if (_instance._slowMoEnabled)
            {
                _instance.SlowmoSnapshot.TransitionTo(0.5f);
            }
            else
            {
                _instance.NormalTimeSnapshot.TransitionTo(0.5f);
            }
        }
	}
}
