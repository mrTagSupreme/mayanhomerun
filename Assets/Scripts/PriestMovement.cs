﻿using UnityEngine;
using System.Collections;

public class PriestMovement : MonoBehaviour {

    public float MaxX;
    public float MinX;
    public float MovementSpeed;
    public float Acceleration;

	public GameObject BatBack;
	public GameObject batHand;

    private Rigidbody2D rigidBody;
    private float currentMovementSpeed;

	protected Animator _animator;

	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody2D>();
		_animator = GetComponentInChildren<Animator> ();
	}

    // Update is called once per frame
    void FixedUpdate()
    {
		if (enabled == true && MayaSettings.playerMovementEnabled == true) {
			float movementSpeedTarget = Input.GetAxis ("Horizontal") * MovementSpeed;
			float acceleration = Acceleration;
			if (movementSpeedTarget != 0 && currentMovementSpeed != 0 && Mathf.Sign (movementSpeedTarget) != Mathf.Sign (currentMovementSpeed))
				acceleration *= 2.0f;
			if (currentMovementSpeed < movementSpeedTarget)
				currentMovementSpeed += acceleration * Time.fixedDeltaTime;
			else if (currentMovementSpeed > movementSpeedTarget)
				currentMovementSpeed -= acceleration * Time.fixedDeltaTime;

			if (movementSpeedTarget == 0 && Mathf.Abs (currentMovementSpeed) < Acceleration * Time.fixedDeltaTime)
				currentMovementSpeed = 0;
			currentMovementSpeed = Mathf.Clamp (currentMovementSpeed, -MovementSpeed, MovementSpeed);

			_animator.SetFloat ("Movement", currentMovementSpeed);

			if (currentMovementSpeed != 0) {
				float newX = rigidBody.position.x + currentMovementSpeed * Time.fixedDeltaTime;
				if (newX < MinX) {
					newX = MinX;
					currentMovementSpeed = 0;
				} else if (newX > MaxX) {
					newX = MaxX;
					currentMovementSpeed = 0;
				}
				rigidBody.MovePosition (new Vector2 (newX, rigidBody.position.y));
			}
		}
	}
}
