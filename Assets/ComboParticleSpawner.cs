﻿using UnityEngine;
using System.Collections;

public class ComboParticleSpawner : MonoBehaviour {

	public GameObject comboParticle;

	public void SpawnCombo(int times, Vector3 position)
	{
		GameObject go = Instantiate<GameObject> (comboParticle);
		go.transform.position = position;


		TextMesh text = go.GetComponent<TextMesh> ();

		if (text != null)
			text.text = "+ " + times;

		ComboParticle particle = go.GetComponent<ComboParticle> ();
		particle.direction = new Vector3(Random.value * 0.3f, 1f, 0f);
	}
}
