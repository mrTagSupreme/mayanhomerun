﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpawnedTextHUD : MonoBehaviour {

	protected EnemySpawner _spawner;
	protected Text _text;

	// Use this for initialization
	void Start () {
		_spawner = GameObject.FindObjectOfType<EnemySpawner> ();
		_text = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		_text.text = _spawner.totalStarted + " / " + MayaSettings.instance.maxEnemies;
	}
}
